#include <SPI.h>
#include <Ethernet.h>
#include <PubSubClient.h>
#include <DMXSerial.h>
#include <FastLED.h>

// CONFIG:
// Update these with values suitable for your network.
#define MQTT_PORT 1883
#define MQTT_USERNAME PSTR("lights")
#define MQTT_PASSWORD PSTR("lights")
#define MQTT_CLIENT_ID PSTR("LitchiDMX_1")
#define MAX_SCENE_NAME_SIZE 16
#define DMXSERIAL_MAX 128  // Up to 512, does not reduce DMXSerials internal buffer size of 512
byte mac[] = { 0xDE, 0x4D, 0xB5, 0xDE, 0xCE, 0x8D };
IPAddress server(192, 168, 1, 114);

// GLOBALS:
EthernetClient ethClient;
PubSubClient client(ethClient);
uint8_t dmxDataSrc[DMXSERIAL_MAX + 1];
uint8_t dmxDataDest[DMXSERIAL_MAX + 1];
fract8 dmxDataFract[DMXSERIAL_MAX + 1];
int dmxMaxChannel = 0;  // DMXSerials internal is initialized to 32
uint8_t fadeAmount = 2;
uint8_t mainSrc = 255;
uint8_t mainDest = 255;
fract8 mainFrac = 0;
char scene[MAX_SCENE_NAME_SIZE + 1] = { '\0' };

void subscene(bool subscribe) {
  if (scene[0] != '\0') {
    char buf[50];
    // Check if strcpy and strcat would produce less program memory
    //snprintf(buf, sizeof(buf), "lights/v2/scenes/%s/channels/+", scene);
    strcpy(buf, PSTR("lights/v2/scenes/"));
    strcat(buf, scene);
    strcat(buf, PSTR("/channels/+"));
    if (subscribe) {
      client.subscribe(buf);
    } else {
      client.unsubscribe(buf);
    }
  }
}

void callback(char* topic, byte* payload, unsigned int length) {
  char* payload_c = (char*)payload;
  payload_c[length] = '\0';
  char* channelTopic = strrchr(topic, '/');
  if (channelTopic == 0) {
    return;
  }
  ++channelTopic;

  if (strcmp(channelTopic, "fade_speed")) {
    fadeAmount = (uint8_t)atoi(payload_c);
  } else if (strcmp(channelTopic, "main")) {
    mainSrc = mainCur();
    mainDest = (uint8_t)atoi(payload_c);
    mainFrac = 0;
  } else if (strcmp(channelTopic, "scene")) {
    subscene(true);
    if (length <= MAX_SCENE_NAME_SIZE) {
      strncpy(scene, payload_c, length);
      subscene(false);
    }
  } else {
    char *topicPos, *payloadPos;
    channelTopic = strtok_r(channelTopic, ",", &topicPos);
    payload_c = strtok_r(payload_c, ",", &payloadPos);
    while (channelTopic != NULL && payload_c != NULL) {
      int channel = atoi(channelTopic);
      uint8_t value = (uint8_t)atoi(payload_c);
      dmxDataDest[channel] = value;
      dmxDataSrc[channel] = dmxDataCur[channel];
      dmxDataFract[channel] = 0;
      if (channel > dmxMaxChannel) {
        dmxMaxChannel = channel;
      }
      channelTopic = strtok_r(NULL, ",", &topicPos);
      payload_c = strtok_r(NULL, ",", &payloadPos);
    }
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    // Attempt to connect
    if (client.connect(MQTT_CLIENT_ID, MQTT_USERNAME, MQTT_PASSWORD, PSTR("lights/v2/health"), 1, true, PSTR("offline"), false)) {
      // Once connected, publish an announcement...
      client.publish(PSTR("lights/v2/health"), PSTR("online"), 1, true);
      // ... and resubscribe
      client.subscribe(PSTR("lights/v2/fade_speed"));
      client.subscribe(PSTR("lights/v2/main"));
      client.subscribe(PSTR("lights/v2/scene"));
    } else {
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

uint8_t dmxDataCur(int channel) {
  if (dmxDataFract[channel] == 255) return dmxDataDest[channel];

  fract8 ease = ease8InOutApprox(dmxDataFract[channel]);
  return lerp8by8(dmxDataSrc[channel], dmxDataDest[channel], ease);
}

uint8_t mainCur() {
  if (mainFrac == 255) return mainDest;

  fract8 ease = ease8InOutApprox(mainFrac);
  return lerp8by8(mainSrc, mainDest, ease);
}

uint8_t dmxDataCurScaled(int channel) {
  return scale8_video(dmxDataCur(channel), mainCur());
}

void light_loop() {
  EVERY_N_MILLISECONDS(24) {
    for (int channel = 0; channel < dmxMaxChannel; channel++) {
      // Increment fading color:
      if (dmxDataFract[channel] < 255) {
        dmxDataFract[channel] = add8(dmxDataFract[channel], fadeAmount);
      }
      // Increment fading main:
      if (mainFrac < 255) {
        mainFrac = add8(mainFrac, fadeAmount);
      }

      DMXSerial.write(channel, dmxDataCurScaled(channel));
    }
  }
}

void setup() {
  DMXSerial.init(DMXController);
  delay(500);

  client.setServer(server, MQTT_PORT);
  client.setCallback(callback);

  Ethernet.begin(mac);
  // Allow the hardware to sort itself out
  delay(1500);
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  light_loop();
}