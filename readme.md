# litchidmx

A simple mqtt dmx controller.

## Setup

Grab an ESP8266, ESP32, or an Arduino with an ethernet shield and open the code by using the Arduino IDE, change your mqtt broker and flash it. Then solder an dmx shield with or without isolation: http://www.mathertel.de/Arduino/DMXShield.aspx

Then connect lights.

## Usage

Take one of the MQTT Clients for your PC or Smartphone and subscribe to `lights/v2/health` to see if your controller is connected and healthy. To set the dmx channels publish a value to a channel topic `lights/v1/scenes/[SCENE]/channels/[CHANNEL]`. Use comma seperation to send multiple at once. This is useful for RGB lights. Here is an example:

- lights/v2/health
- lights/v2/scene = myscene
- lights/v2/scenes/myscene/channels/12 = 255
- lights/v2/scenes/myscene/channels/12,13,14 = 255,255,255

Use retained messages to save your state at the broker and let the controller auto-reload it after reset or power off.

### LitchiLyrics

Download the app, connect to your broker, activate the lights page and controll your lights.

## Api

MQTT Api endpoint reference.

### V1 (For reference only)

- lights/v1/health
- lights/v1/channels/[CHANNEL]

### V2

- Health: `lights/v2/health`
- Channels into scenes: `lights/v2/scenes/[SCENE]/channels/[CHANNEL]`
- An active scene topic: `lights/v2/scene`
- A fade speed: `lights/v2/fade_speed`
- A main brightness: `lights/v2/main`

## Planned Features

- Add channel names: `lights/v1/channel_names/[CHANNEL]`